#!/usr/bin/env python

#program info
__title__ = "Yeah Boi Generator"
__author__ = "Peter Sz."
__licence__ = "GPLv3"
__version__ = "0.6"


#modules
import time

#graphics
says = "(^_^) < "
shouts = "(*o*) < "
disappointed = "(-_-) < "

#boi length limits
hardlimit = 3000 #refuses to perform yeah boi beyond this value
drinklimit = 2000 #asks for a drink first
remarklimit = 420 #says it's going to be a long one


def welcome():
    #Welcome message
    print "Welcome to %s v%s." % (__title__, __version__)
    print
    time.sleep(2)
    print "Use your keyboard to enter values and press ENTER to submit."
    print
    time.sleep(1)
    boiprompt()

def boiprompt():
    #Prompts the user for a numerical input.
    print says +"How long should the Yeah Boi be?"
    print
    global iNumber
    iNumber = raw_input("Give me a number.")
    if iNumber.isdigit():
        boicheck()
    else:
        print disappointed + "A number, man."
        print
        time.sleep(2)
        boiprompt()
        
def boicheck():
    #check csv file if iNumber is the longest ybe, 
    #if yes, print a message: this is my longest ybe
    boidrink()
    
def boidrink():
    #asks for a drink if iNumber is over drinklimit
    if int(iNumber) > drinklimit and int(iNumber) < hardlimit:
        print says + "Dayum... Do you have my drink ready?"
        print
        time.sleep(2)
        request = raw_input("Press ENTER to give him the Ice Tea.")
        time.sleep(2)
        if request == "":
            print says + "Thanks man. I will fail, just so you know."
            print
            time.sleep(2)
            print "[Sips]"
            print
            time.sleep(2)
            boigen()
        else:
            print says + "Come on..."
            print
            time.sleep(2)
            boiprompt()
    else:
        boigen()
    
def boigen():
    #Generates a Yeah Boi.
    if int(iNumber) > hardlimit:
        print says + "Are you trying to kill me? Jesus..."
        print
        time.sleep(2)
        cont()
    elif int(iNumber) >= remarklimit:
        print says + "That's gonna be a long one..."
        print
        time.sleep(2)
        print "[Breathe in extra hard...]"
        print
        time.sleep(5)
        print shouts + "Yeah Bo%s." % ("i" * int(iNumber))
        print
    else:
        print says + "Aight."
        print
        time.sleep(2)
        print "[Breathe in...]"
        print
        time.sleep(3)
        print shouts + "Yeah Bo%s." % ("i" * int(iNumber))
        print
    time.sleep(3)
    #append iNumber to csv file
    cont()
    
def cont():
    #Asks if user wants to continue (y/n).
    answer = raw_input(says + "Do you want another Yeah Boi? (Y/n)")
    if answer.lower() == "y" or answer == "":
        boiprompt()
    elif answer == "n":
        print says + "Aight. Bye."
        print
        exit()
    else:
        cont()
        

#starts the program
welcome()