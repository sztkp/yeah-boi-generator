# Yeah Boi Generator

*Copyright Peter Szentkiralyi, 2017*

*Distributed under GPLv3 license.*

Basic feature list:

 * asks the user to input a desired length for the Yeah Boi
 * generates and outputs the Yeah Boy of the desired length

To-be included:

 * store the length of Yeah Bois in a file
 * alert the user if it's going to be the longest yeah boi ever
